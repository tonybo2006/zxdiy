const db = wx.cloud.database()
const product = db.collection('shop_product')
const _ = db.command
const dataPageNum = 6
Page({
  data: {
    color: wx.getExtConfigSync().color,
    rankList: ["时间", "销量", "价格"],
    show_type: 0, // 0：块状 1：列表
    orderType: 0, // 0："desc"降序   1："asc"升序
    orderBy: 'createtime', // "rank":综合 "sale":销量 "price":价格
    selectIndex: 0,
    search_text: '',
    product_list: [],
    is_all: 0,
    page: 1,
    dataNum: 0,
    isSearch: 0,
    loadCount: 0,
    showCount: 0
  },
  onLoad: function (options) {
    console.log(123)
    this.setData({
      loadCount: this.data.loadCount + 1
    })
    if (options.text) {
      this.setData({
        search_text: options.text
      })
    }
  },
  onShow: function () {
    console.log(345)
    this.setData({
      showCount: this.data.showCount + 1
    })
    if (this.data.showCount == this.data.loadCount) {
      this._clear();
      this.setData({
        dataNum: 0
      })
      this.getProducts(0);
    }
    var pages = getCurrentPages();
    var currPage = pages[pages.length-1];
    if (currPage.data.req==1){
      this._clear();
      this.setData({
        dataNum: 0
      })
      this.getProducts(0);
      this.setData({
        req: 0
      })
    }
  },
  _clear() {
    this.setData({
      page: 1,
      product_list: [],
      is_all: 0,
      show_type: 0,
      orderType: 0,
      orderBy: 'name'
    })
  },
  bindconfirm(e) {
    this.data.search_text = e.detail;
    this._clear();
    this.getProducts(0);
  },
  getProducts(e) {
    let product_list = this.data.product_list;
    if ("" == this.data.search_text) {
      if (this.data.isSearch == 1)
        this.setData({
          dataNum: 0
        })
      if (e == 3) {
        this.setData({
          dataNum: 0
        })
      }
      db.collection('shop_product').where({diyId:wx.getStorageSync('diyId')}).orderBy(this.data.orderBy, this.data.orderType ? 'asc' : 'desc').skip(this.data.dataNum).limit(dataPageNum).get().then(res => {
        console.log(res.data);
        this.data.page++;
        const DATA = res.data;
        product_list = product_list.concat(DATA);
        this.setData({
          dataNum: this.data.dataNum + dataPageNum
        })
        if (product_list.length != this.data.dataNum) this.data.is_all = 1;
        this.setData({
          page: this.data.page,
          is_all: this.data.is_all,
          product_list: product_list,
          orderType: this.data.orderType
        });
      })
      this.setData({
        isSearch: 0
      })
    } else {
      if (this.data.isSearch == 0) {
        this.setData({
          dataNum: 0
        })
      } else {
        if (e != 2) {
          this.setData({
            dataNum: 0
          })
        }
      }
      if (e == 3) {
        this.setData({
          dataNum: 0
        })
      }
      db.collection('shop_product').where({ diyId: wx.getStorageSync('diyId') }).orderBy(this.data.orderBy, this.data.orderType ? 'asc' : 'desc').skip(this.data.dataNum).limit(dataPageNum).get().then(res => {
        console.log(res);
        const DATA = res.data;
        let mData = [];
        var reg = new RegExp(this.data.search_text);
        for (var i = 0; i < DATA.length; i++) {
          if (DATA[i].name.match(reg)) {
            mData.push(DATA[i])
          }
        }

        this.data.page++;

        product_list = product_list.concat(mData);
        this.setData({
          dataNum: this.data.dataNum + dataPageNum
        })
        if (product_list.length != this.data.dataNum) this.data.is_all = 1;
        this.setData({
          page: this.data.page,
          is_all: this.data.is_all,
          product_list: product_list,
          orderType: this.data.orderType
        });
      })
      this.setData({
        isSearch: 1
      })
    }

  },
  // 切换商品展示状态
  switch_type(e) {
    const type = e.currentTarget.dataset.type;
    // show_type 0：块状 1：列表
    this.setData({
      show_type: type
    });
  },
  changeRank(e) {
    const index = Number(e.currentTarget.dataset.index);
    const type = Number(e.currentTarget.dataset.order_type);
    this.setData({
      page: 1,
      product_list: [],
      is_all: 0,
      selectIndex: index,
      orderBy: index == 0 ? 'name' : index == 1 ? 'sale' : 'price',
      orderType: this.data.selectIndex == index ? (type ? 0 : 1) : type
    })
    this.getProducts(3);
  },
  goProductDetail: function (e) {
    console.log(e.currentTarget.dataset.id);

    wx.navigateTo({

      url: `/pages/shop/product/product-detail/index?id=${e.currentTarget.dataset.id}`
    });
  },
  onPullDownRefresh: function () {
    setTimeout(() => {
      this._clear();
      this.getProducts(1);
      wx.stopPullDownRefresh();
    }, 500)
  },
  onReachBottom: function () {
    if (!this.data.is_all) {
      this.getProducts(2);
    }
  },
  onAddProduct: function (e) {
    wx.navigateTo({
      url: `/pages/manage/product-add/index`
    });
  },
  onDelPro(e){
    console.log(e.currentTarget.dataset.id);
    wx.cloud.callFunction({
      // 云函数名称
      name: 'product-del',
      // 传给云函数的参数
      data: {
        id: e.currentTarget.dataset.id
      },
      complete: res => {
        if (res.errMsg =="cloud.callFunction:ok"){
          wx.showToast({
            title: '数据已被删除',
          })
          this._clear();
          this.setData({
            dataNum: 0
          })
          this.getProducts(0);

        }else
          wx.showToast({
            title: '删除失败，请稍后再试',
            icon:'none'
          })
        console.log('callFunction test result: ', res)
      }
    })
    // wx.navigateTo({

    //   url: `/pages/shop/product/product-detail/index?id=${e.currentTarget.dataset.id}`
    // });
  },
  onUptPro(e){
    console.log(e.currentTarget.dataset.id);
    wx.navigateTo({
      url: `/pages/manage/product-add/index?id=` + e.currentTarget.dataset.id
    });
  }
})