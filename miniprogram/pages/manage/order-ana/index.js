// pages/manage/nav/index.js
var wxCharts = require('../../../utils/wxcharts.js');
var app = getApp();
var lineChart = null;
const util = require("../../../utils/util.js");
const db = wx.cloud.database()
const _ = db.command

Page({
  data: {
    width: 180,
    height: 400,
    newOrderInfo:0,
    allOrderInfo:0
  },
  touchHandler: function (e) {
    console.log(lineChart.getCurrentDataIndex(e));
    lineChart.showToolTip(e, {
      // background: '#7cb5ec',
      format: function (item, category) {
        return category + ' ' + item.name + ':' + item.data
      }
    });
  },
  createSimulationData: function () {
    
    return {
      categories: categories,
      data: data
    } 
  },
  onLoad: function (e) {
    let day = util.formatTime(new Date()).split(" ")[0];
    let year = util.formatTime(new Date()).split("-")[0];
    db.collection('order_ana').where({
      date: _.eq(day),
      diyId: wx.getStorageSync('diyId')
    }).get().then(res => {
      console.log(day)
      console.log(res)
      if(res.data.length>0){
        
          this.setData({
            newOrderInfo: res.data[0].sale
          })
        
      } 
    })//TODO 还需要把退款的减去
    db.collection('order_ana').where({
      date: _.eq(year),
      diyId: wx.getStorageSync('diyId')
    }).get().then(res => {
      console.log(year)
      console.log(res)
      if (res.data.length > 0) {

        this.setData({
          allOrderInfo: res.data[0].sale
        })

      }
    })
    



    var windowWidth = 320;
    try {
      var res = wx.getSystemInfoSync();
      windowWidth = res.windowWidth - 20;
      this.setData({
        width: res.windowWidth,
        height: res.windowHeight
      })
      console.log(windowWidth)
    } catch (e) {
      console.error('getSystemInfoSync failed!');
    }

    var categories = [];
    var data = [];
    var ti = -5;
    var t1 = ti;
    var flag1 = true;
    for (var i = ti; i < 1; i++) {
      categories.push(util.getDay(i));
    }
    db.collection('order_ana').where({
      date: _.gte(util.getDay(t1)),
      diyId: wx.getStorageSync('diyId')
    }).get().then(res => {
      if (res.data.length > 0) {
        for (var i = ti; i < 1; i++){
          let f = false;
          for (var i2 = 0; i2 < res.data.length ; i2++) {
            if (util.getDay(i) == res.data[i2].date){
              f = true;
              data.push(res.data[i2].sale);
            
            }
          }
          if(f==false){
            data.push(0)
          }
          
        }
        console.log(data)
        
      }else{
        for (var i = ti; i < 1; i++) {
          data.push(0);
        }
      }

      lineChart = new wxCharts({
        canvasId: 'lineCanvas',
        type: 'line',
        categories: categories,
        animation: true,
        // background: '#f5f5f5',
        series: [{
          name: '交易额',
          data: data,
          format: function (val, name) {
            return val.toFixed(2) + '元';
          }
        }],
        xAxis: {
          disableGrid: true
        },
        yAxis: {
          title: '成交金额 (元)',
          format: function (val) {
            return val.toFixed(2);
          },
          min: 0
        },
        width: windowWidth,
        height: 200,
        dataLabel: false,
        dataPointShape: true,
        extra: {
          lineStyle: 'curve'
        }
      });
    })
  }
})